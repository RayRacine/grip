#lang scribble/manual

@title{Control}
@author[@author+email["Ray Racine" "ray@racine.cloud"]]
@author[@author+email["Georges Dupéron" "georges.duperon@gmail.com"]]

Original code from Georges Dupéron licensed under CC0 when this work was derived.

@include-section["conditionals.scrbl"]
