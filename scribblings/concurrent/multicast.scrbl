#lang scribble/manual

@(require (for-label racket/base
                     racket/contract
                     typed/racket/async-channel
                     grip/concurrent/multicast))

@title{Multicast Asynchronous Channels}

@defmodule[grip/concurrent/multicast]

This provides an implementation of @deftech{multicast channels}, an
extension of @tech[#:doc '(lib"scribblings/reference/reference.scrbl")]{asynchronous channels}
that may have multiple recievers. Unlike asynchronous channels, multicast
channels have distinct “server” and “receiver” objects, since
multicast channels create a one-to-many relationship.

Ported from dynamically typed implementation by Alexis King.

@defidform[#:kind "type"  Multicast-Channelof]{
The Multicast-Channelof parameterized by the type of the broadcast message being sent/received. e.g. (Multicast-Channelof String).
}

@defidform[#:kind "type" Multicast-Receiverof]{
The Multicast-Receiverof type that is parameterized by the broadcast message type being received/sent. e.g. (Multicast-Receiverof String).
}

@defproc[(make-multicast-channel) (Multicast-Channelof a)]{
Creates and returns a new Multicast-Channelof instance.
A created instance of Multicast-Channelof weakly holds instances of created Multicast-Receiverof instances allowing for automatic gc'ing and cleanup.
}

@defproc[(make-multicast-receiver [mc (Multicast-Channelof a)]) (Multicast-Receiverof a)]{
Creates a receiver asynchronous channel that receives messages from @racket[mc]. The returned value is
a type alias for a standard @racket[Async-Channelof] from
@racketmodname[typed/racket/async-channel], but it does @italic{not} support @racket[async-channel-put].
Attempting to write to a receiver channel will raise an exception.

The created Multicast-Receiverof instance is weakly held by the Multicast-Channelof instance creator.  When the client caller to this method lets the returned Multicast-Receiverof instance go out of scope it will be gc'd and de-registered from the Multicast-Channelof instance. Explicit closing of a Multicast-Receiverof instance is not required.
}

@defproc[(multicast-channel-put [mc (Multicast-Channelof a)] [v a]) Void]{
Broadcasts @racket[v] of to all receivers listening to @racket[mc].}
